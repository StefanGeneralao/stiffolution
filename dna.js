function DNA(genelength, maxforce, dna1, dna2){
	this.genes = [];
	if(dna1 && dna2){
		var midpoint = floor(random(genelength));
		for(var i = 0; i < genelength; i++){
			if(i < midpoint){
				this.genes.push(dna1.genes[i]);
			}else{
				this.genes.push(dna2.genes[i]);
			}
		}
	}else{
		for(var i = 0; i < genelength; i++){
			var newgene = p5.Vector.random2D();
			newgene.setMag(maxforce);
			this.genes.push(newgene);
		}
	}

	this.mutate = function(mutationsrate){
		for(var i = 0; i < genelength; i++){
			if(random(1) < mutationsrate){
				this.genes[i] = p5.Vector.random2D();
				this.genes[i].setMag(maxforce);
			}
		}
	}
}
