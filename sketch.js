var defaultLifespan;
var target;
var obstacles;
var population;
var tick;

function setup(){
    frameRate(30);

    // Environment setup.
    createCanvas(windowWidth, windowHeight);
    background(color(10));
    defaultLifespan = height * 0.4;

    // Target setup.
    target = createVector(width * 0.5, height * 0.1);

    // Obstacles setup.
    obstacles = [];
    for(var i = 0; i < 25; i++){
        var tempobstacle = new Obstacle();
        tempobstacle.setLeftBorder(width * 0.1);
        tempobstacle.setRightBorder(width * 0.9);
        tempobstacle.setTopBorder(height * 0.4);
        tempobstacle.setBottomBorder(height * 0.8);
        tempobstacle.setSize(width * random(0.04, 0.08));
        tempobstacle.setJitter(width * 0.0005);
        tempobstacle.show();
        obstacles.push(tempobstacle);
    }

    population = [];
    for(var i = 0; i < 1; i++){
        var temppop = new Population();
        temppop.setSpawnPoint(width * 0.5, height * 0.9);
        temppop.setMutationRate(0.0005);
        temppop.setMaxForce(0.2);
        temppop.setMaxVelocity(1/0);
        temppop.setPopulationSize(200);
        temppop.setCreatureSize(height * 0.001);
        temppop.setChallangerRate(0.0);
        temppop.spawn();
        population.push(temppop);
    }

    // Misc.
    tick = 0;
}

function draw(){
    if(tick < defaultLifespan){ // Phase 1: Move obstacle and creatures.
        // Move population.
        for(var i = 0; i < population.length; i++){
            population[i].move();
            population[i].show();
        }

        // Move obstacles.
        for(var i = 0; i < obstacles.length; i++){
            var tempobstacle = obstacles[i];
            tempobstacle.move();
            tempobstacle.show();
            tempobstacle.updateColor();
        }

        // Draw target.
        push();
        fill(255);
        ellipse(target.x, target.y, width * 0.025);
        pop();
    }else if(tick < defaultLifespan + 10){ // Phase 2: Freeze a short while.

    } else if(tick < defaultLifespan + 30){ // Phase 3: Fade environment.
        if(tick % 2 == 0){
            fadeEnvironment();
        }
    }else{ // Last phase: Reset.
        for(var i = 0; i < population.length; i++){
            population[i].createNewGeneration();
        }
        tick = -1;
    }

    tick++;
}

function fadeEnvironment(){
    push();
    noStroke();
    fill(0, 20);
    rect(0, 0, width, height);
    pop();
}
