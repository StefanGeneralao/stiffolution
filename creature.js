function Creature(spawnpoint, dna){
	this.position = spawnpoint.copy();
	this.colorcode = color(
		random(127, 255), // Red.
		random(0, 200), // Green.
		random(0, 200), // Blue.
		255 // Alpha.
	);
	this.size = width * 0.005;
	this.dna = dna;
	this.fitness = 0;
	this.crashed = false;
	this.completed = false;
	this.velocity = createVector();
	this.finishingtick = defaultLifespan;

	this.copy = function(){
		var tempcreature = new Creature(this.position, this.dna);
		tempcreature.size = this.size;
		tempcreature.finishingtick;

		return tempcreature;
	}

	this.show = function(colorcode){
		push();
		if(colorcode){
			fill(colorcode);
		}else{
			fill(this.colorcode);
		}
		strokeWeight(1);
		stroke(0, 0);
		translate(this.position.x, this.position.y);
		rotate(this.velocity.heading());
		ellipse(0, 0, this.velocity.mag() * 10, this.size);
		pop();
	}

	this.setSize = function(size){
		this.size = size;
	}

	this.setPosition = function(position){
		this.position = position;
	}

	this.move = function(geneindex, maxvelocity){
		// Update velocity vector from dna instruction.
		this.velocity.add(this.dna.genes[geneindex]);

		// Limit velocity.
		if(this.velocity.mag() > maxvelocity){
			this.velocity.setMag(maxvelocity);
		}

		// Update position from velocity.
		this.position.add(this.velocity);
	}

	this.calculateFitness = function(){
		if(this.crashed){
			this.fitness = 0;
		}else{
			var temp = dist(
				this.position.x, this.position.y,
				target.x, target.y
			);
			temp = map(temp, 0, height, height, 0);
			temp = pow(temp, 9) / pow(10, 20);
			temp = round(temp);

			this.fitness += temp;
		}
	}

	this.checkCrashed = function(){
		// Check left wall.
		if(this.position.x < 0){
			this.crashed = true;
			this.finishingtick = tick;
			this.flash();
			return;
		}

		// Check right wall.
		if(this.position.x > width){
			this.crashed = true;
			this.finishingtick = tick;
			this.flash();
			return;
		}

		// Check obstacles.
		for(var i = 0; i < obstacles.length; i++){
			// Get distance to every obstacle.
			var distanceToObstacle = dist(
				obstacles[i].position.x, obstacles[i].position.y,
				this.position.x, this.position.y
			);

			// Set this.crashed to true if obstacle is hit.
			if(distanceToObstacle < obstacles[i].size * 0.5){
				this.crashed = true;
				this.finishingtick = tick;

				// Flash creature.
				this.flash();

				// Flash obstacle.
				noFill();
				stroke(255, 0, 0, 127);
				strokeWeight(4);
				ellipse(obstacles[i].position.x, obstacles[i].position.y, obstacles[i].size * 1);
				pop();

				return;
			}
		}
	}

	this.flash = function(){
		push();
		//fill(255, 255, 255, 255);
		fill(this.colorcode);
		noStroke();
		var flashsize = this.size * this.velocity.mag();
		ellipse(this.position.x, this.position.y, flashsize * 10);
	}

	this.checkCompleted = function(){
		// Get distance to target.
		var distanceToTarget = dist(
			this.position.x, this.position.y,
			target.x, target.y
		);

		// Set this.completed to true if target is hit.
		if(distanceToTarget < width * 0.025 / 2){
			this.completed = true;
			this.finishingtick = tick;
		}
	}

	this.scrambleColor = function(){
		this.colorcode.levels[0] += floor(random(-20, 20));
		this.colorcode.levels[1] += floor(random(-20, 20));
		this.colorcode.levels[2] += floor(random(-20, 20));
		this.colorcode.levels[0] = constrain(this.colorcode.levels[0], 50, 255);
		this.colorcode.levels[1] = constrain(this.colorcode.levels[1], 50, 255);
		this.colorcode.levels[2] = constrain(this.colorcode.levels[2], 50, 255);
	}

	this.colorCopy = function(){
		var tempcolor = color(
			this.colorcode.levels[0],
			this.colorcode.levels[1],
			this.colorcode.levels[2],
			this.colorcode.levels[3]
		);

		return tempcolor;
	}
}
