function Obstacle(){
	// Default members.
	this.leftborder = width * 0.3;
	this.rightborder = width * 0.7;
	this.topborder = height * 0.3;
	this.bottomborder = height * 0.7;
	this.position = createVector(
		random(this.leftborder, this.rightborder),
		random(this.topborder, this.bottomborder)
	);
	this.size = width * 0.1;
	this.jitter = 0.8;
	this.colorcode = color(0, 0, 0, 3);

	// Object functions //////////////////////////////////////////

	// Returns true if obstacle is within border, else false.
	this.isWithinBorder = function(){
		if(this.position.x < this.leftborder){
			return false;
		}else if(this.position.x > this.rightborder){
			return false;
		}

		if(this.position.y < this.topborder){
			return false;
		}else if(this.position.y > this.bottomborder){
			return false;
		}

		return true;
	}

	// Sets borders.
	this.setTopBorder = function(value){
		this.topborder = value;
		this.setRandomPosition();
	}
	this.setRightBorder = function(value){
		this.rightborder = value;
		this.setRandomPosition();
	}
	this.setBottomBorder = function(value){
		this.bottomborder = value;
		this.setRandomPosition();
	}
	this.setLeftBorder = function(value){
		this.leftborder = value;
		this.setRandomPosition();
	}

	// Sets a new random position within border.
	this.setRandomPosition = function(){
		this.position = createVector(
			random(this.leftborder, this.rightborder),
			random(this.topborder, this.bottomborder)
		);
	}

	// Set obstacle diameter.
	this.setSize = function(size){
		this.size = size;
	}

	// Set jitter activity, i.e. speed.
	this.setJitter = function(jitter){
		this.jitter = jitter;
	}

	// Moves obstacle relative to current position.
	// If no argument is given, then jitter a random direction.
	// This function checks if obstacle is within borders.
	this.move = function(relativex, relativey){
		// Move.
		if(relativex && relativey){
			this.position.x += relativex;
			this.position.y += relativey;
		}else{
			this.position.x += random(-this.jitter, this.jitter);
			this.position.y += random(-this.jitter, this.jitter);
		}

		// Constrain position within borders.
		this.position.x = constrain(
			this.position.x,
			this.leftborder,
			this.rightborder
		);
		this.position.y = constrain(
			this.position.y,
			this.topborder,
			this.bottomborder
		);
	}

	// Displays position.
	this.show = function(){
		push();

		// Outer faded ring.
		noFill();
		stroke(this.colorcode);
		strokeWeight(15);
		ellipse(this.position.x, this.position.y, this.size);

		// Inner sharp ring.
		noFill();
		stroke(
			this.colorcode.levels[0],
			this.colorcode.levels[1],
			this.colorcode.levels[2],
			50
		);
		strokeWeight(2);
		ellipse(this.position.x, this.position.y, this.size);

		// Inner black spot.
		fill(color(0, 55));
		noStroke();
		ellipse(this.position.x, this.position.y, this.size);
		pop();
	}

	this.updateColor = function(){
		this.colorcode.levels[0] += round(random(-20, 20));
		this.colorcode.levels[0] = constrain(
			this.colorcode.levels[0],
			0,
			50
		);
		this.colorcode.levels[1] += round(random(-20, 20));
		this.colorcode.levels[1] = constrain(
			this.colorcode.levels[1],
			50,
			255
		);
		this.colorcode.levels[2] += round(random(-20, 20));
		this.colorcode.levels[2] = constrain(
			this.colorcode.levels[2],
			50,
			255
		);
	}
}
