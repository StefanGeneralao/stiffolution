function Population(){
	// Default values.
	this.spawnpoint = createVector(width * 0.5, height * 0.9);
	this.mutationsrate = 0.001;
	this.maxforce = 0.4;
	this.maxvelocity = 4.0;
	this.populationsize = 100;
	this.creaturesize = width * 0.005;
	this.creatures = [];
	this.challangerrate = 0.1;

	this.setSpawnPoint = function(xpos, ypos){
		this.spawnpoint = createVector(xpos, ypos);
	}

	this.setMutationRate = function(mutationsrate){
		this.mutationsrate = mutationsrate;
	}

	this.setMaxForce = function(maxforce){
		this.maxforce = maxforce;
	}

	this.setMaxVelocity = function(maxvelocity){
		this.maxvelocity = maxvelocity;
	}

	this.setPopulationSize = function(populationsize){
		this.populationsize = populationsize;
	}

	this.setCreatureSize = function(creaturesize){
		this.creaturesize = creaturesize;
	}

	this.spawn = function(){
		for(var i = 0; i < this.populationsize; i++){
			var newcreature = new Creature(
				this.spawnpoint,
				new DNA(
					defaultLifespan,
					this.maxforce
				)
			);
			newcreature.size = this.creaturesize;
			this.creatures.push(newcreature);
		}
	}

	this.reset = function(){
		for(var i = 0; i < this.populationsize; i++){
			this.creatures[i].setPosition(this.spawnpoint.copy());
			this.creatures[i].fitness = 0;
		}
	}

	this.move = function(){
		for(var i = 0; i < this.populationsize; i++){
			if(this.creatures[i].crashed){
			}else if(this.creatures[i].completed){
				this.creatures[i].calculateFitness();
			}else{
				this.creatures[i].move(tick, this.maxvelocity);
				this.creatures[i].checkCrashed();
				this.creatures[i].checkCompleted();
				this.creatures[i].calculateFitness();
			}
		}
	}

	this.show = function(){
		for(var i = 0; i < this.populationsize; i++){
			if(!this.creatures[i].crashed){
				this.creatures[i].show();
			}
		}
	}

	this.createNewGeneration = function(){
		// Normalize fitness.
		var maxfit = 0;
		var bestcreature;
		for(var i = 0; i < this.populationsize; i++){
			if(this.creatures[i].completed){
				this.creatures[i].fitness *= 10;
			}
			if(this.creatures[i].fitness > maxfit){
				maxfit = this.creatures[i].fitness;
			}
		}
		for(var i = 0; i < this.populationsize; i++){
			this.creatures[i].fitness /= maxfit;
			if(this.creatures[i].fitness == 1){
				this.creatures[i].fitness *= 10;
				bestcreature = this.creatures[i];
			}else{
				this.creatures[i].fitness *= 10;
			}

			this.creatures[i].fitness = floor(this.creatures[i].fitness);
		}
		console.log(maxfit);

		// Show best creature.
		if(bestcreature){
			bestcreature.position = this.spawnpoint.copy();
			bestcreature.velocity = createVector(0, 0);
			for(var i = 0; i < bestcreature.finishingtick; i++){
				bestcreature.move(i, this.maxvelocity);
				bestcreature.show(color('white'));
			}
		}
		// Create mating pool.
		var matingpool = [];
		for(var i = 0; i < this.populationsize; i++){
			var tempcreature = this.creatures[i];
			for(var j = 0; j < tempcreature.fitness; j++){
				matingpool.push(tempcreature);
			}
		}

		// Update population.
		if(bestcreature){
			bestcreature.position = this.spawnpoint.copy();
			bestcreature.velocity = createVector(0, 0);
			this.creatures[0] = bestcreature;
		}
		for(var i = 1; i < this.populationsize; i++){
			if(matingpool.length == 0){
				for(var j = 0; j < this.populationsize; j++){
					this.creatures[j] = new Creature(
						this.spawnpoint,
						new DNA(defaultLifespan,
						this.maxforce
						)
					);
					this.creatures[j].size = this.creaturesize;
				}
			}else if(random(1) > this.challangerrate){
				// Pick parents and get their dna.
				var parent1 = matingpool[floor(random(matingpool.length))];
				var parent2 = matingpool[floor(random(matingpool.length))];

				// Combine and mutate child dna.
				var newdna = new DNA(
					defaultLifespan,
					this.maxforce,
					parent1.dna,
					parent2.dna
				);
				newdna.mutate(this.mutationsrate);

				// Insert new dna into new creature.
				this.creatures[i] = new Creature(
					this.spawnpoint,
					newdna
				);

				// Set size and color to new creature.
				this.creatures[i].size = this.creaturesize;
				this.creatures[i].colorcode = parent1.colorCopy();
				this.creatures[i].scrambleColor();
			}else{
				this.creatures[i] = new Creature(
					this.spawnpoint,
					new DNA(
						defaultLifespan,
						this.maxforce
					)
				);
				this.creatures[i].size = this.creaturesize;
			}
		}
	}

	this.setChallangerRate = function(challangerrate){
		this.challangerrate = challangerrate;
	}
}
